package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LifeFrame extends JFrame {
    private static final Dimension MAIN_FRAME_MINIMAL_SIZE = new Dimension(800, 600);
    private static final Dimension MAIN_FRAME_DEFAULT_SIZE = new Dimension(800, 600);
    public static final String MAIN_FRAME_HEAD_TEXT = "FIT_16202_Ermakova_Life";
    private static final String MAIN_FRAME_ICON = "icons/main.png";
    private static final String NEW_DOCUMENT_ICON = "icons/newdocument.png";
    private static final String OPEN_ICON = "icons/open.png";
    private static final String SAVE_ICON = "icons/save.png";
    private static final String SAVE_AS_ICON = "icons/saveas.png";
    private static final String CLEAR_ICON = "icons/clear.png";
    private static final String PARAMETERS_ICON = "icons/parameters.png";
    private static final String START_ICON = "icons/start.png";
    private static final String STOP_ICON = "icons/stop.png";
    private static final String STEP_ICON = "icons/step.png";
    private static final String ABOUT_ICON = "icons/about.png";

    private static final String REPLACE_BUTTON_TEXT = "Replace";
    private static final String XOR_BUTTON_TEXT = "XOR";
    private static final String IMPACT_BUTTON_TEXT = "Impact";

    private LifeGameMVC mvc;
    private JScrollPane scrollPane;

    LifeFrame(LifeGameMVC mvc) {
        super();
        this.mvc = mvc;
        scrollPane = null;

        setSize(MAIN_FRAME_DEFAULT_SIZE);
        setMinimumSize(MAIN_FRAME_MINIMAL_SIZE);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent arg0) {
                mvc.getModel().offerToSaveFile();
                System.exit(0);
            }
        });
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(MAIN_FRAME_ICON)));
        setLocationRelativeTo(null);
        setResizable(true);
    }

    public void initScrollPane(LifeView view) {
        scrollPane = new JScrollPane(view);
        add(scrollPane, BorderLayout.CENTER);
    }

    public void initToolBar() {
        JToolBar toolBar;
        toolBar = new JToolBar();
        toolBar.setFloatable(false);

        ImageIcon icnNewDocument = new ImageIcon(LifeFrame.class.getResource(NEW_DOCUMENT_ICON));
        JButton btnNewDocument = new JButton(icnNewDocument);
        btnNewDocument.setToolTipText(LifeModel.MENU_NEW_DOCUMENT_NAME);
        btnNewDocument.setActionCommand(LifeModel.MENU_NEW_DOCUMENT_NAME);

        ImageIcon icnOpen = new ImageIcon(LifeFrame.class.getResource(OPEN_ICON));
        JButton btnOpen = new JButton(icnOpen);
        btnOpen.setToolTipText(LifeModel.MENU_OPEN_NAME);
        btnOpen.setActionCommand(LifeModel.MENU_OPEN_NAME);

        ImageIcon icnSave = new ImageIcon(LifeFrame.class.getResource(SAVE_ICON));
        JButton btnSave = new JButton(icnSave);
        btnSave.setToolTipText(LifeModel.MENU_SAVE_NAME);
        btnSave.setActionCommand(LifeModel.MENU_SAVE_NAME);
        mvc.getModel().setSaveButton(btnSave);

        ImageIcon icnSaveAs = new ImageIcon(LifeFrame.class.getResource(SAVE_AS_ICON));
        JButton btnSaveAs = new JButton(icnSaveAs);
        btnSaveAs.setToolTipText(LifeModel.MENU_SAVE_AS_NAME);
        btnSaveAs.setActionCommand(LifeModel.MENU_SAVE_AS_NAME);

        ImageIcon icnClear = new ImageIcon(LifeFrame.class.getResource(CLEAR_ICON));
        JButton btnClear = new JButton(icnClear);
        btnClear.setToolTipText(LifeModel.MENU_CLEAR_NAME);
        btnClear.setActionCommand(LifeModel.MENU_CLEAR_NAME);

        Dimension size = new Dimension(60, 44);

        JToggleButton btnReplace = new JToggleButton(REPLACE_BUTTON_TEXT, true);
        btnReplace.setToolTipText(LifeModel.MENU_REPLACE_NAME);
        btnReplace.setActionCommand(LifeModel.MENU_REPLACE_NAME);
        btnReplace.setMinimumSize(size);
        btnReplace.setMaximumSize(size);
        btnReplace.setPreferredSize(size);
        mvc.getModel().setReplaceToggleButton(btnReplace);

        JToggleButton btnXOR = new JToggleButton(XOR_BUTTON_TEXT);
        btnXOR.setToolTipText(LifeModel.MENU_XOR_NAME);
        btnXOR.setActionCommand(LifeModel.MENU_XOR_NAME);
        btnXOR.setMinimumSize(size);
        btnXOR.setMaximumSize(size);
        btnXOR.setPreferredSize(size);
        mvc.getModel().setXORToggleButton(btnXOR);

        ButtonGroup bg = new ButtonGroup();
        bg.add(btnReplace);
        bg.add(btnXOR);

        JToggleButton btnImpact = new JToggleButton(IMPACT_BUTTON_TEXT);
        btnImpact.setToolTipText(LifeModel.MENU_IMPACT_NAME);
        btnImpact.setActionCommand(LifeModel.MENU_IMPACT_NAME);
        btnImpact.setMinimumSize(size);
        btnImpact.setMaximumSize(size);
        btnImpact.setPreferredSize(size);
        mvc.getModel().setImpactToggleButton(btnImpact);

        ImageIcon icnParameters = new ImageIcon(LifeFrame.class.getResource(PARAMETERS_ICON));
        JButton btnParameters = new JButton(icnParameters);
        btnParameters.setToolTipText(LifeModel.MENU_PARAMETERS_NAME);
        btnParameters.setActionCommand(LifeModel.MENU_PARAMETERS_NAME);

        ImageIcon icnStart = new ImageIcon(LifeFrame.class.getResource(START_ICON));
        JButton btnRun = new JButton(icnStart);
        btnRun.setToolTipText(LifeModel.MENU_RUN_NAME);
        btnRun.setActionCommand(LifeModel.MENU_RUN_NAME);
        mvc.getModel().setRunButton(btnRun);

        ImageIcon icnStop = new ImageIcon(LifeFrame.class.getResource(STOP_ICON));
        JButton btnStop = new JButton(icnStop);
        btnStop.setToolTipText(LifeModel.MENU_STOP_NAME);
        btnStop.setActionCommand(LifeModel.MENU_STOP_NAME);
        mvc.getModel().setStopButton(btnStop);

        ImageIcon icnStep = new ImageIcon(LifeFrame.class.getResource(STEP_ICON));
        JButton btnStep = new JButton(icnStep);
        btnStep.setToolTipText(LifeModel.MENU_STEP_NAME);
        btnStep.setActionCommand(LifeModel.MENU_STEP_NAME);
        mvc.getModel().setStepButton(btnStep);

        ImageIcon icnAbout = new ImageIcon(LifeFrame.class.getResource(ABOUT_ICON));
        JButton btnAbout = new JButton(icnAbout);
        btnAbout.setToolTipText(LifeModel.MENU_ABOUT_NAME);
        btnAbout.setActionCommand(LifeModel.MENU_ABOUT_NAME);

        toolBar.add(btnNewDocument);
        toolBar.add(btnOpen);
        toolBar.add(btnSave);
        toolBar.add(btnSaveAs);
        toolBar.addSeparator();
        toolBar.add(btnClear);
        toolBar.add(btnReplace);
        toolBar.add(btnXOR);
        toolBar.add(btnImpact);
        toolBar.add(btnParameters);
        toolBar.addSeparator();
        toolBar.add(btnRun);
        toolBar.add(btnStop);
        toolBar.add(btnStep);
        toolBar.addSeparator();
        toolBar.add(btnAbout);

        btnNewDocument.addActionListener(mvc.getController());
        btnOpen.addActionListener(mvc.getController());
        btnSave.addActionListener(mvc.getController());
        btnSaveAs.addActionListener(mvc.getController());
        btnClear.addActionListener(mvc.getController());
        btnReplace.addActionListener(mvc.getController());
        btnXOR.addActionListener(mvc.getController());
        btnImpact.addActionListener(mvc.getController());
        btnParameters.addActionListener(mvc.getController());
        btnRun.addActionListener(mvc.getController());
        btnStop.addActionListener(mvc.getController());
        btnStep.addActionListener(mvc.getController());
        btnAbout.addActionListener(mvc.getController());

        getContentPane().add(toolBar, BorderLayout.NORTH);
    }

    public void initMenu() {
        JMenuBar jmb = new JMenuBar();

        JMenu jmFile = new JMenu(LifeModel.MENU_FILE_NAME);
        jmFile.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiNewDocument = new JMenuItem(LifeModel.MENU_NEW_DOCUMENT_NAME);
        jmiNewDocument.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiOpen = new JMenuItem(LifeModel.MENU_OPEN_NAME);
        jmiOpen.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiSave = new JMenuItem(LifeModel.MENU_SAVE_NAME);
        jmiSave.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setSaveItem(jmiSave);
        JMenuItem jmiSaveAs = new JMenuItem(LifeModel.MENU_SAVE_AS_NAME);
        jmiSaveAs.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiExit = new JMenuItem(LifeModel.MENU_EXIT_NAME);
        jmiExit.setFont(mvc.getModel().getDefaultFont());
        jmFile.add(jmiNewDocument);
        jmFile.add(jmiOpen);
        jmFile.add(jmiSave);
        jmFile.add(jmiSaveAs);
        jmFile.addSeparator();
        jmFile.add(jmiExit);

        JMenu jmEdit = new JMenu(LifeModel.MENU_EDIT_NAME);
        jmEdit.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiClear = new JMenuItem(LifeModel.MENU_CLEAR_NAME);
        jmiClear.setFont(mvc.getModel().getDefaultFont());
        JRadioButtonMenuItem jmiReplace = new JRadioButtonMenuItem(LifeModel.MENU_REPLACE_NAME, true);
        jmiReplace.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setReplaceRadioButton(jmiReplace);
        JRadioButtonMenuItem jmiXOR = new JRadioButtonMenuItem(LifeModel.MENU_XOR_NAME);
        jmiXOR.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setXORRadioButton(jmiXOR);
        ButtonGroup bg = new ButtonGroup();
        bg.add(jmiReplace);
        bg.add(jmiXOR);
        JCheckBoxMenuItem jmiImpact = new JCheckBoxMenuItem(LifeModel.MENU_IMPACT_NAME);
        jmiImpact.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setImpactCheckBox(jmiImpact);
        JMenuItem jmiParameters = new JMenuItem(LifeModel.MENU_PARAMETERS_NAME);
        jmiParameters.setFont(mvc.getModel().getDefaultFont());

        jmEdit.add(jmiClear);
        jmEdit.addSeparator();
        jmEdit.add(jmiReplace);
        jmEdit.add(jmiXOR);
        jmEdit.addSeparator();
        jmEdit.add(jmiImpact);
        jmEdit.addSeparator();
        jmEdit.add(jmiParameters);

        JMenu jmGame = new JMenu(LifeModel.MENU_GAME_NAME);
        jmGame.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiRun = new JMenuItem(LifeModel.MENU_RUN_NAME);
        jmiRun.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setRunItem(jmiRun);
        JMenuItem jmiStop = new JMenuItem(LifeModel.MENU_STOP_NAME);
        jmiStop.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setStopItem(jmiStop);
        JMenuItem jmiStep = new JMenuItem(LifeModel.MENU_STEP_NAME);
        jmiStep.setFont(mvc.getModel().getDefaultFont());
        mvc.getModel().setStepItem(jmiStep);
        jmGame.add(jmiRun);
        jmGame.add(jmiStop);
        jmGame.add(jmiStep);

        JMenu jmHelp = new JMenu(LifeModel.MENU_HELP_NAME);
        jmHelp.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiRules = new JMenuItem(LifeModel.MENU_RULES_NAME);
        jmiRules.setFont(mvc.getModel().getDefaultFont());
        JMenuItem jmiAbout = new JMenuItem(LifeModel.MENU_ABOUT_NAME);
        jmiAbout.setFont(mvc.getModel().getDefaultFont());
        jmHelp.add(jmiRules);
        jmHelp.addSeparator();
        jmHelp.add(jmiAbout);

        jmb.add(jmFile);
        jmb.add(jmEdit);
        jmb.add(jmGame);
        jmb.add(jmHelp);

        jmiNewDocument.addActionListener(mvc.getController());
        jmiOpen.addActionListener(mvc.getController());
        jmiSave.addActionListener(mvc.getController());
        jmiSaveAs.addActionListener(mvc.getController());
        jmiExit.addActionListener(mvc.getController());
        jmiClear.addActionListener(mvc.getController());
        jmiReplace.addActionListener(mvc.getController());
        jmiXOR.addActionListener(mvc.getController());
        jmiImpact.addActionListener(mvc.getController());
        jmiParameters.addActionListener(mvc.getController());
        jmiRun.addActionListener(mvc.getController());
        jmiStop.addActionListener(mvc.getController());
        jmiStep.addActionListener(mvc.getController());
        jmiRules.addActionListener(mvc.getController());
        jmiAbout.addActionListener(mvc.getController());

        setJMenuBar(jmb);
    }
}
