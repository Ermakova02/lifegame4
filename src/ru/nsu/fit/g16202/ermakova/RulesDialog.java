package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class RulesDialog extends JOptionPane {
    private LifeGameMVC mvc;
    private JTextArea text;

    RulesDialog(LifeGameMVC mvc) {
        super();
        this.mvc = mvc;
        text = new JTextArea();
        text.setFont(mvc.getModel().getDefaultFont());
        text.setEditable(false);
        text.setBackground(getBackground());
        text.setForeground(Color.BLACK);
        readText();
        UIManager.put("OptionPane.buttonFont", mvc.getModel().getDefaultFont());
    }

    private void readText() {
        FileInputStream inFile = null;
        String str = new String("");
        try {
            inFile = new FileInputStream(mvc.getModel().RULES_FILE_NAME);
            byte[] buf = new byte[inFile.available()];
            inFile.read(buf);
            str = new String(buf, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        text.setText(str);
    }

    public void showDialog() {
        showMessageDialog(mvc.getFrame(), text, LifeModel.RULES_DIALOG_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }
}
