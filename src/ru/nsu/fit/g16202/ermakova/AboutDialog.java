package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;

public class AboutDialog extends JOptionPane {
    private static final String AUTHOR_ICON = "icons/katya.png";

    private LifeGameMVC mvc;
    private JLabel messageLabel;
    private Icon photoIcon;


    AboutDialog(LifeGameMVC mvc) {
        super();
        this.mvc = mvc;
        messageLabel = new JLabel();
        messageLabel.setFont(mvc.getModel().getDefaultFont());
        messageLabel.setText(LifeModel.ABOUT_DIALOG_MESSAGE);
        photoIcon = new ImageIcon(AboutDialog.class.getResource(AUTHOR_ICON));
        UIManager.put("OptionPane.buttonFont", mvc.getModel().getDefaultFont());
    }
    public void showDialog() {
        showMessageDialog(mvc.getFrame(), messageLabel, LifeModel.ABOUT_DIALOG_TITLE, JOptionPane.PLAIN_MESSAGE, photoIcon);
    }
}
