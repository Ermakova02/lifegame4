package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ParametersDialog extends JDialog {
    private static final String PARAMETERS_DIALOG_TITLE = "Параметры модели";
    private static final String FIELD_SIZE_LABEL_TEXT = "Размер поля";
    private static final String COLUMNS_NUMBER_LABEL_TEXT = "Количество столбцов";
    private static final String ROWS_NUMBER_LABEL_TEXT = "Количество строк";
    private static final String CELL_SIZE_LABEL_TEXT = "Размер ячейки";
    private static final String LINE_WEIGHT_LABEL_TEXT = "Толщина линии";
    private static final String TIMER_LABEL_TEXT = "Таймер";
    private static final String ALGORITHM_PARAMETERS_LABEL_TEXT = "Параметры алгоритма";
    private static final String LIVE_BEGIN_LABEL_TEXT = "LIVE_BEGIN";
    private static final String BIRTH_BEGIN_LABEL_TEXT = "BIRTH_BEGIN";
    private static final String BIRTH_END_LABEL_TEXT = "BIRTH_END";
    private static final String LIVE_END_LABEL_TEXT = "LIVE_END";
    private static final String FST_IMPACT_LABEL_TEXT = "FST_IMPACT";
    private static final String SND_IMPACT_LABEL_TEXT = "SND_IMPACT";
    private static final String MODE_LABEL_TEXT = "Режим";
    private static final String REPLACE_RADIOBUTTON_TEXT = "Replace";
    private static final String XOR_RADIOBUTTON_TEXT = "XOR";
    private static final String OK_BUTTON_TEXT = "OK";
    private static final String CANCEL_BUTTON_TEXT = "Отмена";
    private static final String ERROR_TITLE = "Ошибка";
    private static final String NUMBER_FORMAT_ERROR_MESSAGE = "Ошибка формата при вводе числа";
    private static final String NUMBER_RANGE_ERROR_MESSAGE = "Ошибка диапазона при вводе числа";
    private static final String NUMBER_RULES_ERROR_MESSAGE = "Ошибка условий на параметры алгоритма";
    public static final int COLUMNS_NUMBER_MIN_VALUE = 2;
    public static final int COLUMNS_NUMBER_MAX_VALUE = 100;
    public static final int ROWS_NUMBER_MIN_VALUE = 2;
    public static final int ROWS_NUMBER_MAX_VALUE = 100;
    public static final int CELL_SIZE_MIN_VALUE = 4;
    public static final int CELL_SIZE_MAX_VALUE = 80;
    private static final int TIMER_MIN_VALUE = 50;
    private static final int TIMER_MAX_VALUE = 5000;
    public static final int LINE_WEIGHT_MIN_VALUE = 1;
    public static final int LINE_WEIGHT_MAX_VALUE = 12;
    private static final double LIVE_BEGIN_MIN_VALUE = 0.0;
    private static final double LIVE_BEGIN_MAX_VALUE = 100.0;
    private static final double BIRTH_BEGIN_MIN_VALUE = 0.0;
    private static final double BIRTH_BEGIN_MAX_VALUE = 100.0;
    private static final double BIRTH_END_MIN_VALUE = 0.0;
    private static final double BIRTH_END_MAX_VALUE = 100.0;
    private static final double LIVE_END_MIN_VALUE = 0.0;
    private static final double LIVE_END_MAX_VALUE = 100.0;
    private static final double FST_IMPACT_MIN_VALUE = 0.0;
    private static final double FST_IMPACT_MAX_VALUE = 100.0;
    private static final double SND_IMPACT_MIN_VALUE = 0.0;
    private static final double SND_IMPACT_MAX_VALUE = 100.0;

    private static final Dimension PARAMETERS_DIALOG_DIMENSION = new Dimension(550, 550);

    private static final Point FIELD_SIZE_LABEL_POSITION = new Point(6, 8);
    private static final Dimension FIELD_SIZE_LABEL_SIZE = new Dimension(150, 28);

    private static final Point COLUMNS_NUMBER_LABEL_POSITION = new Point(6, 42);
    private static final Dimension COLUMNS_NUMBER_LABEL_SIZE = new Dimension(150, 28);
    private static final Point COLUMNS_NUMBER_TEXT_POSITION = new Point(162, 42);
    private static final Dimension COLUMNS_NUMBER_TEXT_SIZE = new Dimension(60, 28);
    private static final Point COLUMNS_NUMBER_SLIDER_POSITION = new Point(234, 42);
    private static final Dimension COLUMNS_NUMBER_SLIDER_SIZE = new Dimension(150, 28);

    private static final Point ROWS_NUMBER_LABEL_POSITION = new Point(6, 76);
    private static final Dimension ROWS_NUMBER_LABEL_SIZE = new Dimension(150, 28);
    private static final Point ROWS_NUMBER_TEXT_POSITION = new Point(162, 76);
    private static final Dimension ROWS_NUMBER_TEXT_SIZE = new Dimension(60, 28);
    private static final Point ROWS_NUMBER_SLIDER_POSITION = new Point(234, 76);
    private static final Dimension ROWS_NUMBER_SLIDER_SIZE = new Dimension(150, 28);

    private static final Point CELL_SIZE_LABEL_POSITION = new Point(6, 110);
    private static final Dimension CELL_SIZE_LABEL_SIZE = new Dimension(150, 28);
    private static final Point CELL_SIZE_TEXT_POSITION = new Point(162, 110);
    private static final Dimension CELL_SIZE_TEXT_SIZE = new Dimension(60, 28);
    private static final Point CELL_SIZE_SLIDER_POSITION = new Point(234, 110);
    private static final Dimension CELL_SIZE_SLIDER_SIZE = new Dimension(150, 28);

    private static final Point LINE_WEIGHT_LABEL_POSITION = new Point(6, 144);
    private static final Dimension LINE_WEIGHT_LABEL_SIZE = new Dimension(150, 28);
    private static final Point LINE_WEIGHT_TEXT_POSITION = new Point(162, 144);
    private static final Dimension LINE_WEIGHT_TEXT_SIZE = new Dimension(60, 28);
    private static final Point LINE_WEIGHT_SLIDER_POSITION = new Point(234, 144);
    private static final Dimension LINE_WEIGHT_SLIDER_SIZE = new Dimension(150, 28);

    private static final Point TIMER_LABEL_POSITION = new Point(6, 178);
    private static final Dimension TIMER_LABEL_SIZE = new Dimension(150, 28);
    private static final Point TIMER_TEXT_POSITION = new Point(162, 178);
    private static final Dimension TIMER_TEXT_SIZE = new Dimension(60, 28);
    private static final Point TIMER_SLIDER_POSITION = new Point(234, 178);
    private static final Dimension TIMER_SLIDER_SIZE = new Dimension(150, 28);

    private static final Point ALGORITHM_PARAMETERS_LABEL_POSITION = new Point(6, 212);
    private static final Dimension ALGORITHM_PARAMETERS_LABEL_SIZE = new Dimension(150, 28);

    private static final Point LIVE_BEGIN_LABEL_POSITION = new Point(6, 246);
    private static final Dimension LIVE_BEGIN_LABEL_SIZE = new Dimension(150, 28);
    private static final Point LIVE_BEGIN_TEXT_POSITION = new Point(162, 246);
    private static final Dimension LIVE_BEGIN_TEXT_SIZE = new Dimension(60, 28);

    private static final Point BIRTH_BEGIN_LABEL_POSITION = new Point(6, 280);
    private static final Dimension BIRTH_BEGIN_LABEL_SIZE = new Dimension(150, 28);
    private static final Point BIRTH_BEGIN_TEXT_POSITION = new Point(162, 280);
    private static final Dimension BIRTH_BEGIN_TEXT_SIZE = new Dimension(60, 28);

    private static final Point BIRTH_END_LABEL_POSITION = new Point(6, 314);
    private static final Dimension BIRTH_END_LABEL_SIZE = new Dimension(150, 28);
    private static final Point BIRTH_END_TEXT_POSITION = new Point(162, 314);
    private static final Dimension BIRTH_END_TEXT_SIZE = new Dimension(60, 28);

    private static final Point LIVE_END_LABEL_POSITION = new Point(6, 348);
    private static final Dimension LIVE_END_LABEL_SIZE = new Dimension(150, 28);
    private static final Point LIVE_END_TEXT_POSITION = new Point(162, 348);
    private static final Dimension LIVE_END_TEXT_SIZE = new Dimension(60, 28);

    private static final Point FST_IMPACT_LABEL_POSITION = new Point(6, 382);
    private static final Dimension FST_IMPACT_LABEL_SIZE = new Dimension(150, 28);
    private static final Point FST_IMPACT_TEXT_POSITION = new Point(162, 382);
    private static final Dimension FST_IMPACT_TEXT_SIZE = new Dimension(60, 28);

    private static final Point SND_IMPACT_LABEL_POSITION = new Point(6, 416);
    private static final Dimension SND_IMPACT_LABEL_SIZE = new Dimension(150, 28);
    private static final Point SND_IMPACT_TEXT_POSITION = new Point(162, 416);
    private static final Dimension SND_IMPACT_TEXT_SIZE = new Dimension(60, 28);

    private static final Point MODE_LABEL_POSITION = new Point(300, 212);
    private static final Dimension MODE_LABEL_SIZE = new Dimension(100, 28);
    private static final Point REPLACE_RADIOBUTTON_POSITION = new Point(300, 246);
    private static final Dimension REPLACE_RADIOBUTTON_SIZE = new Dimension(100, 28);
    private static final Point XOR_RADIOBUTTON_POSITION = new Point(300, 280);
    private static final Dimension XOR_RADIOBUTTON_SIZE = new Dimension(100, 28);
    private static final Point OK_BUTTON_POSITION = new Point(171, 460);
    private static final Dimension OK_BUTTON_SIZE = new Dimension(96, 32);
    private static final Point CANCEL_BUTTON_POSITION = new Point(283, 460);
    private static final Dimension CANCEL_BUTTON_SIZE = new Dimension(96, 32);

    LifeGameMVC mvc;
    private JLabel fieldSizeLabel;
    private JLabel columnsNumberLabel;
    private JTextField columnsNumberText;
    private JSlider columnsNumberSlider;
    private JLabel rowsNumberLabel;
    private JTextField rowsNumberText;
    private JSlider rowsNumberSlider;
    private JLabel cellSizeLabel;
    private JTextField cellSizeText;
    private JSlider cellSizeSlider;
    private JLabel lineWeightLabel;
    private JTextField lineWeightText;
    private JSlider lineWeightSlider;
    private JLabel timerLabel;
    private JTextField timerText;
    private JSlider timerSlider;
    private JLabel algorithmParametersLabel;
    private JLabel liveBeginLabel;
    private JTextField liveBeginText;
    private JLabel birthBeginLabel;
    private JTextField birthBeginText;
    private JLabel birthEndLabel;
    private JTextField birthEndText;
    private JLabel liveEndLabel;
    private JTextField liveEndText;
    private JLabel fstImpactLabel;
    private JTextField fstImpactText;
    private JLabel sndImpactLabel;
    private JTextField sndImpactText;
    private JLabel modeLabel;
    private JRadioButton replaceButton;
    private JRadioButton xorButton;
    private JButton okButton;
    private JButton cancelButton;

    private boolean okBtnPressed;

    ParametersDialog(LifeGameMVC mvc, Frame owner) {
        super(owner, PARAMETERS_DIALOG_TITLE, true);
        setSize(PARAMETERS_DIALOG_DIMENSION);
        setLayout(null);
        setResizable(false);
        this.mvc = mvc;
        okBtnPressed = false;

        fieldSizeLabel = new JLabel(FIELD_SIZE_LABEL_TEXT);
        fieldSizeLabel.setFont(mvc.getModel().getDefaultFont());
        fieldSizeLabel.setLocation(FIELD_SIZE_LABEL_POSITION);
        fieldSizeLabel.setSize(FIELD_SIZE_LABEL_SIZE);

        columnsNumberLabel = new JLabel(COLUMNS_NUMBER_LABEL_TEXT);
        columnsNumberLabel.setFont(mvc.getModel().getDefaultFont());
        columnsNumberLabel.setLocation(COLUMNS_NUMBER_LABEL_POSITION);
        columnsNumberLabel.setSize(COLUMNS_NUMBER_LABEL_SIZE);

        columnsNumberText = new JTextField();
        columnsNumberText.setFont(mvc.getModel().getDefaultFont());
        columnsNumberText.setLocation(COLUMNS_NUMBER_TEXT_POSITION);
        columnsNumberText.setSize(COLUMNS_NUMBER_TEXT_SIZE);

        BoundedRangeModel brmModel1 = new DefaultBoundedRangeModel(COLUMNS_NUMBER_MIN_VALUE, 1, COLUMNS_NUMBER_MIN_VALUE, COLUMNS_NUMBER_MAX_VALUE + 1);
        columnsNumberSlider = new JSlider(brmModel1);
        columnsNumberSlider.setLocation(COLUMNS_NUMBER_SLIDER_POSITION);
        columnsNumberSlider.setSize(COLUMNS_NUMBER_SLIDER_SIZE);

        columnsNumberSlider.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                columnsNumberText.setText(String.valueOf(columnsNumberSlider.getValue()));
            }
        });
        columnsNumberText.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent ke) {
                String typed = columnsNumberText.getText();
                columnsNumberSlider.setValue(0);
                if(!typed.matches("\\d+") || typed.length() > 3) {
                    return;
                }
                int value = Integer.parseInt(typed);
                columnsNumberSlider.setValue(value);
            }
        });

        rowsNumberLabel = new JLabel(ROWS_NUMBER_LABEL_TEXT);
        rowsNumberLabel.setFont(mvc.getModel().getDefaultFont());
        rowsNumberLabel.setLocation(ROWS_NUMBER_LABEL_POSITION);
        rowsNumberLabel.setSize(ROWS_NUMBER_LABEL_SIZE);

        rowsNumberText = new JTextField();
        rowsNumberText .setFont(mvc.getModel().getDefaultFont());
        rowsNumberText .setLocation(ROWS_NUMBER_TEXT_POSITION);
        rowsNumberText .setSize(ROWS_NUMBER_TEXT_SIZE);

        BoundedRangeModel brmModel2 = new DefaultBoundedRangeModel(ROWS_NUMBER_MIN_VALUE, 1, ROWS_NUMBER_MIN_VALUE, ROWS_NUMBER_MAX_VALUE + 1);
        rowsNumberSlider = new JSlider(brmModel2);
        rowsNumberSlider.setLocation(ROWS_NUMBER_SLIDER_POSITION);
        rowsNumberSlider.setSize(ROWS_NUMBER_SLIDER_SIZE);

        rowsNumberSlider.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                rowsNumberText.setText(String.valueOf(rowsNumberSlider.getValue()));
            }
        });
        rowsNumberText.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent ke) {
                String typed = rowsNumberText.getText();
                rowsNumberSlider.setValue(0);
                if(!typed.matches("\\d+") || typed.length() > 3) {
                    return;
                }
                int value = Integer.parseInt(typed);
                rowsNumberSlider.setValue(value);
            }
        });

        cellSizeLabel = new JLabel(CELL_SIZE_LABEL_TEXT);
        cellSizeLabel.setFont(mvc.getModel().getDefaultFont());
        cellSizeLabel.setLocation(CELL_SIZE_LABEL_POSITION);
        cellSizeLabel.setSize(CELL_SIZE_LABEL_SIZE);

        cellSizeText = new JTextField();
        cellSizeText.setFont(mvc.getModel().getDefaultFont());
        cellSizeText.setLocation(CELL_SIZE_TEXT_POSITION);
        cellSizeText.setSize(CELL_SIZE_TEXT_SIZE);

        BoundedRangeModel brmModel3 = new DefaultBoundedRangeModel(CELL_SIZE_MIN_VALUE, 1, CELL_SIZE_MIN_VALUE, CELL_SIZE_MAX_VALUE + 1);
        cellSizeSlider = new JSlider(brmModel3);
        cellSizeSlider.setLocation(CELL_SIZE_SLIDER_POSITION);
        cellSizeSlider.setSize(CELL_SIZE_SLIDER_SIZE);

        cellSizeSlider.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                cellSizeText.setText(String.valueOf(cellSizeSlider.getValue()));
            }
        });
        cellSizeText.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent ke) {
                String typed = cellSizeText.getText();
                cellSizeSlider.setValue(0);
                if(!typed.matches("\\d+") || typed.length() > 3) {
                    return;
                }
                int value = Integer.parseInt(typed);
                cellSizeSlider.setValue(value);
            }
        });

        lineWeightLabel = new JLabel(LINE_WEIGHT_LABEL_TEXT);
        lineWeightLabel.setFont(mvc.getModel().getDefaultFont());
        lineWeightLabel.setLocation(LINE_WEIGHT_LABEL_POSITION);
        lineWeightLabel.setSize(LINE_WEIGHT_LABEL_SIZE);

        lineWeightText = new JTextField();
        lineWeightText.setFont(mvc.getModel().getDefaultFont());
        lineWeightText.setLocation(LINE_WEIGHT_TEXT_POSITION);
        lineWeightText.setSize(LINE_WEIGHT_TEXT_SIZE);

        BoundedRangeModel brmModel4 = new DefaultBoundedRangeModel(LINE_WEIGHT_MIN_VALUE, 1, LINE_WEIGHT_MIN_VALUE, LINE_WEIGHT_MAX_VALUE + 1);
        lineWeightSlider = new JSlider(brmModel4);
        lineWeightSlider.setLocation(LINE_WEIGHT_SLIDER_POSITION);
        lineWeightSlider.setSize(LINE_WEIGHT_SLIDER_SIZE);

        lineWeightSlider.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                lineWeightText.setText(String.valueOf(lineWeightSlider.getValue()));
            }
        });
        lineWeightText.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent ke) {
                String typed = lineWeightText.getText();
                lineWeightSlider.setValue(0);
                if(!typed.matches("\\d+") || typed.length() > 3) {
                    return;
                }
                int value = Integer.parseInt(typed);
                lineWeightSlider.setValue(value);
            }
        });

        timerLabel = new JLabel(TIMER_LABEL_TEXT);
        timerLabel.setFont(mvc.getModel().getDefaultFont());
        timerLabel.setLocation(TIMER_LABEL_POSITION);
        timerLabel.setSize(TIMER_LABEL_SIZE);

        timerText = new JTextField();
        timerText.setFont(mvc.getModel().getDefaultFont());
        timerText.setLocation(TIMER_TEXT_POSITION);
        timerText.setSize(TIMER_TEXT_SIZE);

        BoundedRangeModel brmModel5 = new DefaultBoundedRangeModel(TIMER_MIN_VALUE, 1, TIMER_MIN_VALUE, TIMER_MAX_VALUE + 1);
        timerSlider = new JSlider(brmModel5);
        timerSlider.setLocation(TIMER_SLIDER_POSITION);
        timerSlider.setSize(TIMER_SLIDER_SIZE);

        timerSlider.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                timerText.setText(String.valueOf(timerSlider.getValue()));
            }
        });
        timerText.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent ke) {
                String typed = timerText.getText();
                timerSlider.setValue(0);
                if(!typed.matches("\\d+") || typed.length() > 3) {
                    return;
                }
                int value = Integer.parseInt(typed);
                timerSlider.setValue(value);
            }
        });

        algorithmParametersLabel = new JLabel(ALGORITHM_PARAMETERS_LABEL_TEXT);
        algorithmParametersLabel.setFont(mvc.getModel().getDefaultFont());
        algorithmParametersLabel.setLocation(ALGORITHM_PARAMETERS_LABEL_POSITION);
        algorithmParametersLabel.setSize(ALGORITHM_PARAMETERS_LABEL_SIZE);

        liveBeginLabel = new JLabel(LIVE_BEGIN_LABEL_TEXT);
        liveBeginLabel.setFont(mvc.getModel().getDefaultFont());
        liveBeginLabel.setLocation(LIVE_BEGIN_LABEL_POSITION);
        liveBeginLabel.setSize(LIVE_BEGIN_LABEL_SIZE);

        liveBeginText = new JTextField();
        liveBeginText.setFont(mvc.getModel().getDefaultFont());
        liveBeginText.setLocation(LIVE_BEGIN_TEXT_POSITION);
        liveBeginText.setSize(LIVE_BEGIN_TEXT_SIZE);

        birthBeginLabel = new JLabel(BIRTH_BEGIN_LABEL_TEXT);
        birthBeginLabel.setFont(mvc.getModel().getDefaultFont());
        birthBeginLabel.setLocation(BIRTH_BEGIN_LABEL_POSITION);
        birthBeginLabel.setSize(BIRTH_BEGIN_LABEL_SIZE);

        birthBeginText = new JTextField();
        birthBeginText.setFont(mvc.getModel().getDefaultFont());
        birthBeginText.setLocation(BIRTH_BEGIN_TEXT_POSITION);
        birthBeginText.setSize(BIRTH_BEGIN_TEXT_SIZE);

        birthEndLabel = new JLabel(BIRTH_END_LABEL_TEXT);
        birthEndLabel.setFont(mvc.getModel().getDefaultFont());
        birthEndLabel.setLocation(BIRTH_END_LABEL_POSITION);
        birthEndLabel.setSize(BIRTH_END_LABEL_SIZE);

        birthEndText = new JTextField();
        birthEndText.setFont(mvc.getModel().getDefaultFont());
        birthEndText.setLocation(BIRTH_END_TEXT_POSITION);
        birthEndText.setSize(BIRTH_END_TEXT_SIZE);

        liveEndLabel = new JLabel(LIVE_END_LABEL_TEXT);
        liveEndLabel.setFont(mvc.getModel().getDefaultFont());
        liveEndLabel.setLocation(LIVE_END_LABEL_POSITION);
        liveEndLabel.setSize(LIVE_END_LABEL_SIZE);

        liveEndText = new JTextField();
        liveEndText.setFont(mvc.getModel().getDefaultFont());
        liveEndText.setLocation(LIVE_END_TEXT_POSITION);
        liveEndText.setSize(LIVE_END_TEXT_SIZE);

        fstImpactLabel = new JLabel(FST_IMPACT_LABEL_TEXT);
        fstImpactLabel.setFont(mvc.getModel().getDefaultFont());
        fstImpactLabel.setLocation(FST_IMPACT_LABEL_POSITION);
        fstImpactLabel.setSize(FST_IMPACT_LABEL_SIZE);

        fstImpactText = new JTextField();
        fstImpactText.setFont(mvc.getModel().getDefaultFont());
        fstImpactText.setLocation(FST_IMPACT_TEXT_POSITION);
        fstImpactText.setSize(FST_IMPACT_TEXT_SIZE);

        sndImpactLabel = new JLabel(SND_IMPACT_LABEL_TEXT);
        sndImpactLabel.setFont(mvc.getModel().getDefaultFont());
        sndImpactLabel.setLocation(SND_IMPACT_LABEL_POSITION);
        sndImpactLabel.setSize(SND_IMPACT_LABEL_SIZE);

        sndImpactText = new JTextField();
        sndImpactText.setFont(mvc.getModel().getDefaultFont());
        sndImpactText.setLocation(SND_IMPACT_TEXT_POSITION);
        sndImpactText.setSize(SND_IMPACT_TEXT_SIZE);

        modeLabel = new JLabel(MODE_LABEL_TEXT);
        modeLabel.setFont(mvc.getModel().getDefaultFont());
        modeLabel.setLocation(MODE_LABEL_POSITION);
        modeLabel.setSize(MODE_LABEL_SIZE);

        replaceButton = new JRadioButton(REPLACE_RADIOBUTTON_TEXT);
        replaceButton.setFont(mvc.getModel().getDefaultFont());
        replaceButton.setLocation(REPLACE_RADIOBUTTON_POSITION);
        replaceButton.setSize(REPLACE_RADIOBUTTON_SIZE);

        xorButton = new JRadioButton(XOR_RADIOBUTTON_TEXT);
        xorButton.setFont(mvc.getModel().getDefaultFont());
        xorButton.setLocation(XOR_RADIOBUTTON_POSITION);
        xorButton.setSize(XOR_RADIOBUTTON_SIZE);

        ButtonGroup bg = new ButtonGroup();
        bg.add(replaceButton);
        bg.add(xorButton);

        replaceButton.setSelected(true);

        okButton = new JButton(OK_BUTTON_TEXT);
        okButton.setFont(mvc.getModel().getDefaultFont());
        okButton.setLocation(OK_BUTTON_POSITION);
        okButton.setSize(OK_BUTTON_SIZE);

        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                processOKButton();
            }
        });

        cancelButton = new JButton(CANCEL_BUTTON_TEXT);
        cancelButton.setFont(mvc.getModel().getDefaultFont());
        cancelButton.setLocation(CANCEL_BUTTON_POSITION);
        cancelButton.setSize(CANCEL_BUTTON_SIZE);

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                okBtnPressed = false;
                hideDialog();
            }
        });

        add(fieldSizeLabel);
        add(columnsNumberLabel);
        add(columnsNumberText);
        add(columnsNumberSlider);
        add(rowsNumberLabel);
        add(rowsNumberText);
        add(rowsNumberSlider);
        add(cellSizeLabel);
        add(cellSizeText);
        add(cellSizeSlider);
        add(lineWeightLabel);
        add(lineWeightText);
        add(lineWeightSlider);
        add(timerLabel);
        add(timerText);
        add(timerSlider);
        add(algorithmParametersLabel);
        add(liveBeginLabel);
        add(liveBeginText);
        add(birthBeginLabel);
        add(birthBeginText);
        add(birthEndLabel);
        add(birthEndText);
        add(liveEndLabel);
        add(liveEndText);
        add(fstImpactLabel);
        add(fstImpactText);
        add(sndImpactLabel);
        add(sndImpactText);
        add(modeLabel);
        add(replaceButton);
        add(xorButton);
        add(okButton);
        add(cancelButton);
    }

    public void showDialog() {
        setLocationRelativeTo(getOwner());
        setVisible(true);
    }

    public void processOKButton() {
        double liveBegin, birthBegin, birthEnd, liveEnd, fstImpact, sndImpact;
        int colomnsNum, rowsNum, cellSize, lineThickness, timerValue;
        String errorMessage = null;
        try {
            colomnsNum = Integer.parseInt(columnsNumberText.getText());
            rowsNum = Integer.parseInt(rowsNumberText.getText());
            cellSize = getCellSize();
            lineThickness = getLineThickness();
            timerValue = getTimerValue();
            liveBegin = getLiveBegin();
            birthBegin = getBirthBegin();
            birthEnd = getBirthEnd();
            liveEnd = getLiveEnd();
            fstImpact = getFstImpact();
            sndImpact = getSndImpact();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null,
                    NUMBER_FORMAT_ERROR_MESSAGE,
                    ERROR_TITLE,
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (colomnsNum < COLUMNS_NUMBER_MIN_VALUE || colomnsNum > COLUMNS_NUMBER_MAX_VALUE ||
            rowsNum < ROWS_NUMBER_MIN_VALUE || rowsNum > ROWS_NUMBER_MAX_VALUE ||
            cellSize < CELL_SIZE_MIN_VALUE || cellSize > CELL_SIZE_MAX_VALUE ||
            lineThickness < LINE_WEIGHT_MIN_VALUE || lineThickness > LINE_WEIGHT_MAX_VALUE ||
            timerValue < TIMER_MIN_VALUE || timerValue > TIMER_MAX_VALUE ||
            liveBegin < LIVE_BEGIN_MIN_VALUE || liveBegin > LIVE_BEGIN_MAX_VALUE ||
            birthBegin < BIRTH_BEGIN_MIN_VALUE || birthBegin > BIRTH_BEGIN_MAX_VALUE ||
            birthEnd < BIRTH_END_MIN_VALUE || birthEnd > BIRTH_END_MAX_VALUE ||
            liveEnd < LIVE_END_MIN_VALUE || liveEnd > LIVE_END_MAX_VALUE ||
            fstImpact < FST_IMPACT_MIN_VALUE || fstImpact > FST_IMPACT_MAX_VALUE ||
            sndImpact < SND_IMPACT_MIN_VALUE || sndImpact > SND_IMPACT_MAX_VALUE) {
            JOptionPane.showMessageDialog(null,
                    NUMBER_RANGE_ERROR_MESSAGE,
                    ERROR_TITLE,
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (!(liveBegin >= 0 && liveBegin <= birthBegin && birthBegin <= birthEnd && birthEnd <= liveEnd)) {
            JOptionPane.showMessageDialog(null,
                    NUMBER_RULES_ERROR_MESSAGE,
                    ERROR_TITLE,
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        okBtnPressed = true;
        hideDialog();
    }

    public void setFieldSize(int c, int r) {
        columnsNumberText.setText(String.valueOf(c));
        rowsNumberText.setText(String.valueOf(r));
        columnsNumberSlider.setValue(c);
        rowsNumberSlider.setValue(r);
    }

    public Point getFieldSize() {
        int c = Integer.parseInt(columnsNumberText.getText());
        int r = Integer.parseInt(rowsNumberText.getText());
        return new Point(c, r);
    }

    public void setCellSize(int s) {
        cellSizeText.setText(String.valueOf(s));
        cellSizeSlider.setValue(s);
    }

    public int getCellSize() {
        return Integer.parseInt(cellSizeText.getText());
    }

    public void setLineThickness(int t) {
        lineWeightText.setText(String.valueOf(t));
        lineWeightSlider.setValue(t);
    }

    public int getLineThickness() {
        return Integer.parseInt(lineWeightText.getText());
    }

    public void setTimerValue(int v) {
        timerText.setText(String.valueOf(v));
    }

    public int getTimerValue() {
        return Integer.parseInt(timerText.getText());
    }

    public void setAlgorithmParams(double lb, double bb, double be, double le, double fi, double si) {
        liveBeginText.setText(String.valueOf(lb));
        birthBeginText.setText(String.valueOf(bb));
        birthEndText.setText(String.valueOf(be));
        liveEndText.setText(String.valueOf(le));
        fstImpactText.setText(String.valueOf(fi));
        sndImpactText.setText(String.valueOf(si));
    }

    public double getLiveBegin() {
        return Double.parseDouble(liveBeginText.getText());
    }

    public double getBirthBegin() {
        return Double.parseDouble(birthBeginText.getText());
    }

    public double getBirthEnd() {
        return Double.parseDouble(birthEndText.getText());
    }

    public double getLiveEnd() {
        return Double.parseDouble(liveEndText.getText());
    }

    public double getFstImpact() {
        return Double.parseDouble(fstImpactText.getText());
    }

    public double getSndImpact() {
        return Double.parseDouble(sndImpactText.getText());
    }

    public void setMode(boolean replaceMode) {
        replaceButton.setSelected(replaceMode);
        xorButton.setSelected(!replaceMode);
    }

    public boolean isReplaceMode() {
        return replaceButton.isSelected();
    }

    public boolean okPressed() { return okBtnPressed; }
    private void hideDialog() {
        setVisible(false);
    }
}
