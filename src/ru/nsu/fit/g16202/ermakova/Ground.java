package ru.nsu.fit.g16202.ermakova;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Ground {
    private static final int offsets[][][][] = {
            {{{-1, -1}, {0, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}},
            {{0, -2}, {-2, -1}, {1, -1}, {-2, 1}, {1, 1}, {0, 2}}},
            {{{0, -1}, {1, -1}, {-1, 0}, {1, 0}, {0, 1}, {1, 1}},
            {{0, -2}, {-1, -1}, {2, -1}, {-1, 1}, {2, 1}, {0, 2}}}};
    private List<Cell> cells;
    private int width;
    private int height;

    private double liveBegin;
    private double liveEnd;
    private double birthBegin;
    private double birthEnd;
    private double fstImpact;
    private double sndImpact;

    Ground(int width, int height) {
        if (width < 2) width = 2;
        if (height < 2) height = 2;
        init(width, height);
        setParams(LifeModel.DEFAULT_LIVE_BEGIN,
                LifeModel.DEFAULT_BIRTH_BEGIN,
                LifeModel.DEFAULT_BIRTH_END,
                LifeModel.DEFAULT_LIVE_END,
                LifeModel.DEFAULT_FST_IMPACT,
                LifeModel.DEFAULT_SND_IMPACT);
    }

    private String formatImpact(double value) {
        if (value >= 1000.0) return new String("999");
        if (value < 0.1) return new String("0");
        NumberFormat nf = new DecimalFormat("#.######");
        if (value >= 0.1 && value < 1.0) {
            double result = (double) ((int)(value * 10.0)) / 10.0;
            return nf.format(result);
        }
        if (value >= 1.0 && value < 10.0) {
            if (value - (double) ((int)value) < 0.1) return String.format("%d", (int)value);
            double result = (double) ((int)(value * 10.0)) / 10.0;
            return nf.format(result);
        }
        return String.format("%d", (int)value);
    }

    public void clear() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Cell cell = cells.get(y * width + x);
                cell.alive = false;
                cell.value = 0.0;
                cell.needUpdate = true;
                cell.impactText = null;
                cell.textPosition = null;
            }
        }
    }

    public void updateImpactText(Point p) {
        if (!validCoord(p)) return;
        Cell cell = cells.get(p.y * width + p.x);
        cell.impactText = formatImpact(cell.value);
    }

    public String getImpactText(Point p) {
        if (!validCoord(p)) return new String("");
        return cells.get(p.y * width + p.x).impactText;
    }

    public void updateImpactTextPosition(Point p, Rectangle r, FontMetrics fm) {
        if (!validCoord(p)) return;
        Cell cell = cells.get(p.y * width + p.x);
        int x = r.x + ((r.width - fm.stringWidth(cell.impactText)) / 2);
        int y = r.y + (((r.height - fm.getHeight()) / 2) + fm.getAscent());
        cell.textPosition = new Point(x, y);
    }

    public Point getImpactTextPosition(Point p) {
        return cells.get(p.y * width + p.x).textPosition;
    }

    public void init(int width, int height) {
        if (width < 2 || height < 2) return;
        this.width = width;
        this.height = height;
        cells = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells.add(new Cell(false, 0.0));
            }
        }
    }

    public void resize(int nw, int nh) {
        if (width < 2 || height < 2) return;
        List<Cell> newCells = new ArrayList<>();
        for (int j = 0; j < nh; j++) {
            for (int i = 0; i < nw; i++) {
                if (i < width && j < height) {
                    newCells.add(cells.get(j * width + i));
                } else {
                    newCells.add(new Cell(false, 0.0));
                }
            }
        }
        cells = new ArrayList<>(newCells);
        width = nw;
        height = nh;
    }

    public void setParams(double liveBegin, double birthBegin, double birthEnd, double liveEnd, double fstImpact, double sndImpact) {
        this.liveBegin = liveBegin;
        this.liveEnd = liveEnd;
        this.birthBegin = birthBegin;
        this.birthEnd = birthEnd;
        this.fstImpact = fstImpact;
        this.sndImpact = sndImpact;
    }

    public double getValue(Point p) {
        if (!validCoord(p)) return 0.0;
        return cells.get(p.y * width + p.x).value;
    }

    public void setValue(double value, Point p) {
        if (!validCoord(p)) return;
        cells.get(p.y * width + p.x).value = value;
    }

    public Boolean getAlive(Point p) {
        if (!validCoord(p)) return false;
        return cells.get(p.y * width + p.x).alive;
    }

    public void setAlive(Boolean alive, Point p) {
        if (!validCoord(p)) return;
        cells.get(p.y * width + p.x).alive = alive;
    }

    public Boolean getUpdate(Point p) {
        if (!validCoord(p)) return false;
        return cells.get(p.y * width + p.x).needUpdate;
    }

    public void setUpdate(Boolean update, Point p) {
        if (!validCoord(p)) return;
        cells.get(p.y * width + p.x).needUpdate = update;
    }

    public Color getCellColor(Point p) {
        return getAlive(p) ? LifeView.ALIVE_COLOR : LifeView.DEAD_COLOR;
    }

    private boolean validCoord(Point p) {
        if (p.x < 0 || p.y < 0 || p.x >= width || p.y >= height) return false;
        if ((p.y % 2 == 1) && (p.x == width - 1)) return false;
        return true;
    }

    public void calculateImpact() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Point coord = new Point(x, y);
                if (validCoord(coord)) {
                    int count = 0, i = 0, neighborX = 0, neighborY = 0;
                    int line = y % 2;
                    double impact = 0.0;
                    Point neighbor = null;
                    for (i = 0; i < 6; i++) {
                        neighborX = x + offsets[line][0][i][0];
                        neighborY = y + offsets[line][0][i][1];
                        neighbor = new Point(neighborX, neighborY);
                        if (validCoord(neighbor)) {
                            if (getAlive(neighbor) == true) count++;
                        }
                    }
                    impact += (double)count * fstImpact;
                    count = 0;
                    for (i = 0; i < 6; i++) {
                        neighborX = x + offsets[line][1][i][0];
                        neighborY = y + offsets[line][1][i][1];
                        neighbor = new Point(neighborX, neighborY);
                        if (validCoord(neighbor)) {
                            if (getAlive(neighbor) == true) count++;
                        }
                    }
                    impact += (double)count * sndImpact;
                    if (getValue(coord) != impact) setUpdate(true, coord);
                    setValue(impact, coord);
                }
            }
        }
    }

    public Point [] requestNeighbors(Point cell) {
        Point [] neighbors = null;
        if (validCoord(cell)) {
            neighbors = new Point[13];
            int count = 0, i, neighborX, neighborY;
            int line = cell.y % 2;
            Point neighbor;
            neighbors[count] = cell;
            count++;
            for (i = 0; i < 6; i++) {
                neighborX = cell.x + offsets[line][0][i][0];
                neighborY = cell.y + offsets[line][0][i][1];
                neighbor = new Point(neighborX, neighborY);
                if (validCoord(neighbor)) {
                    neighbors[count] = neighbor;
                    count++;
                }
            }
            for (i = 0; i < 6; i++) {
                neighborX = cell.x + offsets[line][1][i][0];
                neighborY = cell.y + offsets[line][1][i][1];
                neighbor = new Point(neighborX, neighborY);
                if (validCoord(neighbor)) {
                    neighbors[count] = neighbor;
                    count++;
                }
            }
            for (i = count; i < 13; i++)
                neighbors[i] = null;
        }
        return neighbors;
    }

    public void next() {
        List<Cell> nextStep = new ArrayList<>(cells);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Point p = new Point(x, y);
                if (validCoord(p)) {
                    double value = getValue(p);
                    if (value >= birthBegin && value <= birthEnd) {
                        if (!getAlive(p)) nextStep.get(y * width + x).needUpdate = true;
                        nextStep.get(y * width + x).alive = true;
                    } else if (value < liveBegin || value > liveEnd) {
                        if (getAlive(p)) nextStep.get(y * width + x).needUpdate = true;
                        nextStep.get(y * width + x).alive = false;
                    }
                }
            }
        }
        cells = new ArrayList<>(nextStep);
        calculateImpact();
    }
}
