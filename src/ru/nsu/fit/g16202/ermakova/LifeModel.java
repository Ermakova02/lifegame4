package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class LifeModel {
    public static final String MENU_FILE_NAME = "Файл";
    public static final String MENU_NEW_DOCUMENT_NAME = "Новый документ";
    public static final String MENU_OPEN_NAME = "Открыть";
    public static final String MENU_SAVE_NAME = "Сохранить";
    public static final String MENU_SAVE_AS_NAME = "Сохранить как";
    public static final String MENU_EXIT_NAME = "Выход";

    public static final String MENU_EDIT_NAME = "Редактировать";
    public static final String MENU_CLEAR_NAME = "Очистить";
    public static final String MENU_REPLACE_NAME = "Replace";
    public static final String MENU_XOR_NAME = "XOR";
    public static final String MENU_IMPACT_NAME = "Отображать Impact";
    public static final String MENU_PARAMETERS_NAME = "Параметры";

    public static final String MENU_GAME_NAME = "Игра";
    public static final String MENU_RUN_NAME = "Пуск";
    public static final String MENU_STOP_NAME = "Стоп";
    public static final String MENU_STEP_NAME = "Шаг";

    public static final String MENU_HELP_NAME = "Помощь";
    public static final String MENU_RULES_NAME = "Правила игры";
    public static final String MENU_ABOUT_NAME = "О программе";

    public static final String ABOUT_DIALOG_MESSAGE = "<html><body>Реализация игры \"Жизнь\" Конвея на шестиугольниках.<br><br>Автор: Екатерина Ермакова.<br><br>Email: ermakova99@gmail.com.<br><br>Факультет Информационных Технологий. НГУ.<br><br>2019 год.</body></html>";
    public static final String ABOUT_DIALOG_TITLE = "О программе FIT_16202_Ermakova_Life";
    public static final String RULES_DIALOG_TITLE = "Правила игры FIT_16202_Ermakova_Life";
    public static final String DATA_FOLDER = ".\\FIT_16202_Ermakova_Life_Data";
    public static final String RULES_FILE_NAME = "FIT_16202_Ermakova_Life_About.txt";
    public static final String DEFAULT_FILE_NAME = "Без имени";
    public static final String ERROR_TITLE = "Ошибка";
    public static final String SAVE_FILE_IO_ERROR = "Ошибка сохранения файла ";
    public static final String OPEN_FILE_IO_ERROR = "Ошибка открытия файла ";
    public static final String FILE_FORMAT_ERROR = "Ошибка в формате файла ";
    public static final String NEED_SAVE_FILE_TITLE = "Сохранение файла";
    public static final String NEED_SAVE_FILE_MESSAGE = "Сохранить текущую модель?";

    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 13;

    private static final String IMPACT_FONT_NAME = "Segoe UI";
    private static final int IMPACT_FONT_SIZE = 12;
    private static final int MINIMAL_IMPACT_CELL_SIZE = 12;

    private static final int DEFAULT_COLUMNS_NUM = 15;
    private static final int DEFAULT_ROWS_NUM = 10;
    private static final int DEFAULT_LINE_THICKNESS = 1;
    private static final int DEFAULT_CELL_SIZE = 20;
    private static final int STEP_DELAY = 500;

    public static final double DEFAULT_LIVE_BEGIN = 2.0;
    public static final double DEFAULT_LIVE_END = 3.3;
    public static final double DEFAULT_BIRTH_BEGIN = 2.3;
    public static final double DEFAULT_BIRTH_END = 2.9;
    public static final double DEFAULT_FST_IMPACT = 1.0;
    public static final double DEFAULT_SND_IMPACT = 0.3;

    private LifeGameMVC mvc;
    private Font defaultFont;
    private Font impactFont;
    private AboutDialog aboutDialog;
    private RulesDialog rulesDialog;
    private ParametersDialog paramDialog;
    private TimerTick tickTask;
    private Timer timer;
    private JFileChooser fileChooser;
    private String currentFolder;

    private int columnsNum;
    private int rowsNum;
    private int lineThickness;
    private int cellSize;

    private int timerValue;

    private double liveBegin;
    private double birthBegin;
    private double birthEnd;
    private double liveEnd;
    private double fstImpact;
    private double sndImpact;

    private String fileName;
    private boolean fileSaved;
    private Ground ground;
    private CellsPointsArea cellsArea;
    private boolean cellsAreaSet;
    private boolean replaceInputMode;
    private boolean showImpact;
    private boolean gameRunned;
    private Point currentDraggedCell;
    private JToggleButton btnReplace;
    private JToggleButton btnXOR;
    private JRadioButtonMenuItem jmiReplace;
    private JRadioButtonMenuItem jmiXOR;
    private JToggleButton btnImpact;
    private JCheckBoxMenuItem jmiImpact;
    private JButton btnSave;
    private JButton btnRun;
    private JMenuItem jmiRun;
    private JButton btnStop;
    private JMenuItem jmiSave;
    private JMenuItem jmiStop;
    private JButton btnStep;
    private JMenuItem jmiStep;

    public void setSaveButton(JButton button) { btnSave = button; }
    public void setReplaceToggleButton(JToggleButton button) { btnReplace = button; }
    public void setXORToggleButton(JToggleButton button) { btnXOR = button; }
    public void setReplaceRadioButton(JRadioButtonMenuItem button) { jmiReplace = button; }
    public void setXORRadioButton(JRadioButtonMenuItem button) { jmiXOR = button; }
    public void setImpactToggleButton(JToggleButton button) { btnImpact = button; }
    public void setImpactCheckBox(JCheckBoxMenuItem item) { jmiImpact = item; }
    public void setRunButton(JButton button) { btnRun = button; }
    public void setStopButton(JButton button) { btnStop = button; }
    public void setStepButton(JButton button) { btnStep = button; }
    public void setSaveItem(JMenuItem item) { jmiSave = item; }
    public void setRunItem(JMenuItem item) { jmiRun = item; }
    public void setStopItem(JMenuItem item) { jmiStop = item; }
    public void setStepItem(JMenuItem item) { jmiStep = item; }

    LifeModel(LifeGameMVC mvc) {
        this.mvc = mvc;
        setDefaults();
        ground = new Ground(columnsNum, rowsNum);
        ground.setParams(liveBegin, birthBegin, birthEnd, liveEnd, fstImpact, sndImpact);
        cellsArea = new CellsPointsArea();
        cellsAreaSet = false;
        replaceInputMode = true;
        showImpact = false;
        gameRunned = false;
        currentDraggedCell = null;
        aboutDialog = null;
        rulesDialog = null;
        paramDialog = null;
        btnReplace = null;
        btnXOR = null;
        jmiReplace = null;
        jmiXOR = null;
        btnImpact = null;
        jmiImpact = null;
        timer = null;
        tickTask = null;
        fileName = DEFAULT_FILE_NAME;
        fileSaved = false;
        fileChooser = new JFileChooser(DATA_FOLDER);
        currentFolder = DATA_FOLDER;
    }

    private void setDefaults() {
        defaultFont = new Font(DEFAULT_FONT_NAME, Font.PLAIN, DEFAULT_FONT_SIZE);
        impactFont = new Font(IMPACT_FONT_NAME, Font.PLAIN, IMPACT_FONT_SIZE);
        columnsNum = DEFAULT_COLUMNS_NUM;
        rowsNum = DEFAULT_ROWS_NUM;
        lineThickness = DEFAULT_LINE_THICKNESS;
        cellSize = DEFAULT_CELL_SIZE;
        timerValue = STEP_DELAY;
        liveBegin = DEFAULT_LIVE_BEGIN;
        birthBegin =DEFAULT_BIRTH_BEGIN;
        birthEnd = DEFAULT_BIRTH_END;
        liveEnd = DEFAULT_LIVE_END;
        fstImpact = DEFAULT_FST_IMPACT;
        sndImpact = DEFAULT_SND_IMPACT;
    }

    public void init() {
        btnStop.setEnabled(false);
        jmiStop.setEnabled(false);
        mvc.getFrame().setTitle(fileName + " - " + mvc.getFrame().MAIN_FRAME_HEAD_TEXT);

    }

    public Font getDefaultFont() { return defaultFont; }

    public Font getImpactFont() { return impactFont; }

    public String getFileName() { return fileName; }

    public Color getCellColor(Point p) {
        return ground.getCellColor(p);
    }
    public boolean getNeedRepaint(Point p) {
        return ground.getUpdate(p);
    }
    public void setNeedRepaint(boolean needRepaint, Point p) {
        ground.setUpdate(needRepaint, p);
    }

    public int getColumnsNum() {return columnsNum; }
    public int getRowsNum() { return rowsNum; }
    public int getLineThickness() {return lineThickness; }
    public int getCellSize() {return cellSize; }

    public boolean isCellsAreaSet() {
        return cellsAreaSet;
    }

    public void setCellsAreaSet(boolean value) {
        cellsAreaSet = value;
    }

    public void setCellsAreaDimension(int width, int height) {
        cellsArea.setDimension(width, height);
    }

    public void setCellPosition(Point coord, Point cell) {
        cellsArea.setCellPosition(coord, cell);
    }

    private Point getCellByPoint(Point p) {
        if (!cellsAreaSet) return null;
        return cellsArea.getCell(p);
    }

    public void updateImpactText(Point cell) {
        ground.updateImpactText(cell);
    }

    public String getImpactText(Point cell) {
        return ground.getImpactText(cell);
    }

    public void updateImpactTextPosition(Point cell, Rectangle cellRect, FontMetrics fm) {
        ground.updateImpactTextPosition(cell, cellRect, fm);
    }

    public Point getImpactTextPosition(Point cell) {
        return ground.getImpactTextPosition(cell);
    }

    public Point [] requestNeighbors(Point cell) {
        return ground.requestNeighbors(cell);
    }

    public void calculateImpact() {
        ground.calculateImpact();
    }

    public void nextStep() {
        ground.next();
    }

    public void toggleAlive(Point cell) {
        ground.setAlive(!ground.getAlive(cell), cell);
    }

    public void processClick(Point p) {
        if (gameRunned) return;
        Point cell = getCellByPoint(p);
        if (cell == null) return;
        if (replaceInputMode)
            if (ground.getAlive(cell)) return;
        setFileNotSaved();
        if (showImpact) {
            mvc.getView().updateImpactNeighbors(cell);
        } else {
            ground.setAlive(!ground.getAlive(cell), cell);
            ground.calculateImpact();
            mvc.getView().repaintCell(cell);
        }
    }

    public void processDrag(Point p) {
        if (gameRunned) return;
        Point cell = getCellByPoint(p);
        if (cell == null) return;
        if (currentDraggedCell == null || (!currentDraggedCell.equals(cell))) {
            if (replaceInputMode)
                if (ground.getAlive(cell)) return;
            currentDraggedCell = cell;
            setFileNotSaved();
            if (showImpact) {
                mvc.getView().updateImpactNeighbors(cell);
            } else {
                ground.setAlive(!ground.getAlive(cell), cell);
                ground.calculateImpact();
                mvc.getView().repaintCell(cell);
            }
        }
    }

    private void saveFile() {
        String fullFileName = currentFolder + "\\" + fileName;
        FileWriter file = null;
        try {
            file = new FileWriter(fullFileName);
            file.write(Integer.toString(columnsNum) + " " + Integer.toString(rowsNum) + "\n");
            file.write(Integer.toString(lineThickness) + "\n");
            file.write(Integer.toString(cellSize) + "\n");
            int count = 0;
            for (int i = 0; i < columnsNum; i++)
                for (int j = 0; j < rowsNum; j++)
                    if (ground.getAlive(new Point(i, j)))
                        count++;
            file.write(Integer.toString(count) + "\n");
            for (int i = 0; i < columnsNum; i++)
                for (int j = 0; j < rowsNum; j++)
                    if (ground.getAlive(new Point(i, j)))
                        file.write(Integer.toString(i) + " " + Integer.toString(j) + "\n");
            file.flush();
            setFileSaved();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,
                    SAVE_FILE_IO_ERROR + fullFileName,
                    ERROR_TITLE,
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void fileFormatErrorMessage(String fileName) {
        JOptionPane.showMessageDialog(null,
                FILE_FORMAT_ERROR + fileName,
                ERROR_TITLE,
                JOptionPane.ERROR_MESSAGE);
    }

    private void openFile(String newFileName, String newFolder) {
        Scanner scanner = null;
        int newColumnsNum = 0, newRowsNum = 0, newLineThickness = 0, newCellSize = 0, cellsNumber = 0;
        List<Point> newCells = null;
        String fullFileName = newFolder + "\\" + newFileName;
        File file = new File(fullFileName);
        try {
            scanner = new Scanner(file);
            newColumnsNum =  scanner.nextInt();
            newRowsNum =  scanner.nextInt();
            if (newColumnsNum < ParametersDialog.COLUMNS_NUMBER_MIN_VALUE || newColumnsNum > ParametersDialog.COLUMNS_NUMBER_MAX_VALUE ||
                    newRowsNum < ParametersDialog.ROWS_NUMBER_MIN_VALUE || newRowsNum > ParametersDialog.ROWS_NUMBER_MAX_VALUE) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            String line = scanner.nextLine();
            line = line.trim();
            if (line.length() != 0 && !line.startsWith("//")) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            newLineThickness =  scanner.nextInt();
            if (newLineThickness < ParametersDialog.LINE_WEIGHT_MIN_VALUE || newLineThickness > ParametersDialog.LINE_WEIGHT_MAX_VALUE) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            line = scanner.nextLine();
            line = line.trim();
            if (line.length() != 0 && !line.startsWith("//")) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            newCellSize =  scanner.nextInt();
            if (newCellSize < ParametersDialog.CELL_SIZE_MIN_VALUE || newCellSize > ParametersDialog.CELL_SIZE_MAX_VALUE) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            line = scanner.nextLine();
            line = line.trim();
            if (line.length() != 0 && !line.startsWith("//")) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            cellsNumber =  scanner.nextInt();
            if (cellsNumber < 0) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            line = scanner.nextLine();
            line = line.trim();
            if (line.length() != 0 && !line.startsWith("//")) {
                fileFormatErrorMessage(newFileName);
                return;
            }
            newCells = new ArrayList<>();
            for (int i = 0; i < cellsNumber; i++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                if (x < 0 || x >= newColumnsNum || y < 0 || y >= newRowsNum) {
                    fileFormatErrorMessage(newFileName);
                    return;
                }
                if (y % 2 == 1 && x == newColumnsNum - 1) {
                    fileFormatErrorMessage(newFileName);
                    return;
                }
                line = scanner.nextLine();
                line = line.trim();
                if (line.length() != 0 && !line.startsWith("//")) {
                    fileFormatErrorMessage(newFileName);
                    return;
                }
                newCells.add(new Point(x, y));
            }
        } catch (IOException e) {
            fileFormatErrorMessage(newFileName);
            return;
        } catch (InputMismatchException e) {
            fileFormatErrorMessage(newFileName);
            return;
        } catch (NoSuchElementException e) {
            fileFormatErrorMessage(newFileName);
            return;
        }
        fileName = newFileName;
        mvc.getFrame().setTitle(fileName + " - " + mvc.getFrame().MAIN_FRAME_HEAD_TEXT);
        columnsNum = newColumnsNum;
        rowsNum = newRowsNum;
        lineThickness = newLineThickness;
        cellSize = newCellSize;
        cellsArea = new CellsPointsArea();
        cellsAreaSet = false;
        if (gameRunned) stopGame();
        ground.clear();
        ground.resize(columnsNum, rowsNum);
        mvc.getView().prepareOutputImage();
        for (int i = 0; i < cellsNumber; i++) {
            ground.setAlive(true, newCells.get(i));
        }
        ground.calculateImpact();
        setFileSaved();
        mvc.getView().repaintAll(showImpact);
    }

    private void setFileNotSaved() {
        fileSaved = false;
        btnSave.setEnabled(true);
    }

    private void setFileSaved() {
        fileSaved = true;
        btnSave.setEnabled(false);
    }

    public void newDocument() {
        offerToSaveFile();
        if (gameRunned) stopGame();
        fileName = DEFAULT_FILE_NAME;
        mvc.getFrame().setTitle(fileName + " - " + mvc.getFrame().MAIN_FRAME_HEAD_TEXT);
        columnsNum = DEFAULT_COLUMNS_NUM;
        rowsNum = DEFAULT_ROWS_NUM;
        lineThickness = DEFAULT_LINE_THICKNESS;
        cellSize = DEFAULT_CELL_SIZE;
        cellsArea = new CellsPointsArea();
        cellsAreaSet = false;
        ground.clear();
        ground.resize(columnsNum, rowsNum);
        mvc.getView().prepareOutputImage();
        ground.calculateImpact();
        setFileSaved();
        mvc.getView().repaintAll(showImpact);
    }

    public void offerToSaveFile() {
        if (!fileSaved) {
            int result = JOptionPane.showConfirmDialog(null,
                    NEED_SAVE_FILE_MESSAGE,
                    NEED_SAVE_FILE_TITLE,
                    JOptionPane.YES_NO_CANCEL_OPTION);
            switch (result) {
                case JOptionPane.YES_OPTION:
                    saveFile();
                    break;
                case JOptionPane.NO_OPTION:
                    break;
                case JOptionPane.CANCEL_OPTION:
                    return;
            }
        }
    }

    public void openDocument() {
        offerToSaveFile();
        fileChooser.setDialogTitle("Открыть файл");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setSelectedFile(new File(fileName));
        int result = fileChooser.showOpenDialog(mvc.getFrame());
        switch (result) {
            case JFileChooser.APPROVE_OPTION:
                File openedFile = fileChooser.getSelectedFile();
                String newFileName = openedFile.getName();
                String newFolder = openedFile.getParent();
                openFile(newFileName, newFolder);
                break;
            case JFileChooser.CANCEL_OPTION:
                break;
            case JFileChooser.ERROR_OPTION:
                JOptionPane.showMessageDialog(null,
                        OPEN_FILE_IO_ERROR + fileName,
                        ERROR_TITLE,
                        JOptionPane.ERROR_MESSAGE);
                break;
        }
    }

    public void saveDocument() {
        saveFile();
    }

    public void saveDocumentAs() {
        fileChooser.setDialogTitle("Сохранить файл как");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setSelectedFile(new File(fileName));
        int result = fileChooser.showSaveDialog(mvc.getFrame());
        switch (result) {
            case JFileChooser.APPROVE_OPTION:
                File savedFile = fileChooser.getSelectedFile();
                fileName = savedFile.getName();
                mvc.getFrame().setTitle(fileName + " - " + mvc.getFrame().MAIN_FRAME_HEAD_TEXT);
                currentFolder = savedFile.getParent();
                saveFile();
                break;
            case JFileChooser.CANCEL_OPTION:
                break;
            case JFileChooser.ERROR_OPTION:
                JOptionPane.showMessageDialog(null,
                        SAVE_FILE_IO_ERROR + fileName,
                        ERROR_TITLE,
                        JOptionPane.ERROR_MESSAGE);
                break;
        }
    }

    public void clearField() {
        if (gameRunned) stopGame();
        ground.clear();
        mvc.getView().repaintAll(showImpact);
    }

    public void replaceMode() {
        if (replaceInputMode) return;
        replaceInputMode = true;
        btnReplace.setSelected(true);
        jmiReplace.setSelected(true);
        btnXOR.setSelected(false);
        jmiXOR.setSelected(false);
    }

    public void xorMode() {
        if (!replaceInputMode) return;
        replaceInputMode = false;
        btnXOR.setSelected(true);
        jmiXOR.setSelected(true);
        btnReplace.setSelected(false);
        jmiReplace.setSelected(false);
    }

    public void impactMode() {
        if (showImpact) {
            showImpact = false;
            btnImpact.setSelected(false);
            jmiImpact.setSelected(false);
            mvc.getView().switchOffImpact();
        } else {
            showImpact = true;
            btnImpact.setSelected(true);
            jmiImpact.setSelected(true);
            mvc.getView().switchOnImpact();
        }
    }

    public void parametersDialog() {
        if (paramDialog == null) paramDialog = new ParametersDialog(mvc, mvc.getFrame());
        paramDialog.setFieldSize(columnsNum, rowsNum);
        paramDialog.setCellSize(cellSize);
        paramDialog.setLineThickness(lineThickness);
        paramDialog.setTimerValue(timerValue);
        paramDialog.setAlgorithmParams(liveBegin, birthBegin, birthEnd, liveEnd, fstImpact, sndImpact);
        paramDialog.setMode(replaceInputMode);
        paramDialog.showDialog();
        if (paramDialog.okPressed()) {
            Point size = paramDialog.getFieldSize();
            columnsNum = size.x;
            rowsNum = size.y;
            cellSize = paramDialog.getCellSize();
            lineThickness = paramDialog.getLineThickness();
            timerValue = paramDialog.getTimerValue();
            liveBegin = paramDialog.getLiveBegin();
            birthBegin = paramDialog.getBirthBegin();
            birthEnd = paramDialog.getBirthEnd();
            liveEnd = paramDialog.getLiveEnd();
            fstImpact = paramDialog.getFstImpact();
            sndImpact = paramDialog.getSndImpact();
            if (paramDialog.isReplaceMode()) replaceMode();
            else xorMode();
            if (gameRunned) {
                if (timer != null) timer.cancel();
            }
            ground.resize(columnsNum, rowsNum);
            ground.setParams(liveBegin, birthBegin, birthEnd, liveEnd, fstImpact, sndImpact);
            mvc.getView().prepareOutputImage();
            cellsAreaSet = false;
            mvc.getView().repaintAll(showImpact);
            if (gameRunned) {
                timer = new Timer(true);
                tickTask = new TimerTick(this);
                timer.schedule(tickTask, timerValue, timerValue);
            }
        }
    }

    public void runGame() {
        gameRunned = true;
        btnRun.setEnabled(false);
        btnStep.setEnabled(false);
        btnStop.setEnabled(true);
        jmiRun.setEnabled(false);
        jmiStep.setEnabled(false);
        jmiStop.setEnabled(true);

        if (timer != null) timer.cancel();
        timer = new Timer(true);
        tickTask = new TimerTick(this);
        timer.schedule(tickTask, timerValue, timerValue);

    }

    public void stopGame() {
        gameRunned = false;
        btnRun.setEnabled(true);
        btnStep.setEnabled(true);
        btnStop.setEnabled(false);
        jmiRun.setEnabled(true);
        jmiStep.setEnabled(true);
        jmiStop.setEnabled(false);
        if (timer != null) timer.cancel();
    }

    public void stepGame() {
        if (showImpact) {
            mvc.getView().repaintImpactStep();
        } else {
            ground.next();
            mvc.getView().repaintPartial();
        }
    }

    public void showRules() {
        if (rulesDialog == null) rulesDialog = new RulesDialog(mvc);
        rulesDialog.showDialog();
    }

    public void showAbout() {
        if (aboutDialog == null) aboutDialog = new AboutDialog(mvc);
        aboutDialog.showDialog();
    }
}
