package ru.nsu.fit.g16202.ermakova;

public class LifeGameMVC {
    private LifeFrame frame = null;
    private LifeModel model = null;
    private LifeView view = null;
    private LifeController controller = null;
    public LifeGameMVC() {
        frame = new LifeFrame(this);
        model = new LifeModel(this);
        view = new LifeView(this);
        controller = new LifeController(this);
        frame.initScrollPane(view);
        frame.initMenu();
        frame.initToolBar();
        model.init();
        frame.setVisible(true);
    }

    public LifeFrame getFrame() { return frame; }
    public LifeModel getModel() { return model; }
    public LifeView getView() { return view;}
    public LifeController getController() { return controller; }
}
