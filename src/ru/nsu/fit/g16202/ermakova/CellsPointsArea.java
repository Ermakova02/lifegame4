package ru.nsu.fit.g16202.ermakova;

import java.awt.*;

public class CellsPointsArea {
    private int width;
    private int height;
    private Point array[][];

    public CellsPointsArea() {
        width = 0;
        height = 0;
        array = null;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        array = new Point[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                array[i][j] = null;
    }

    public void setCellPosition(Point coord, Point cell) {
        array[coord.x][coord.y] = new Point(cell);
    }

    public Point getCell(Point coord) {
        if (coord.x < 0 || coord.x > width - 1 || coord.y < 0 || coord.y > height - 1) return null;
        return array[coord.x][coord.y];
    }
}
