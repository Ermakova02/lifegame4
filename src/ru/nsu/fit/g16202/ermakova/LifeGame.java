package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;

public class LifeGame {
    private static final String SAVE_FILE_TEXT = "Сохранить";
    private static final String OPEN_FILE_TEXT = "Открыть";
    private static final String CANCEL_FILE_TEXT = "Отмена";
    private static final String FILE_NAME_TEXT = "Наименование файла";
    private static final String FILE_TYPES_TEXT = "Типы файлов";
    private static final String FOLDER_NAME_TEXT = "Папка";
    private static final String SAVE_IN_FOLDER_TEXT = "Сохранить в папке";
    private static final String FOLDER_PATH_TEXT = "Путь папки";
    private static final String YES_BUTTON_TEXT = "Да";
    private static final String NO_BUTTON_TEXT = "Нет";
    private static final String CANCEL_BUTTON_TEXT = "Отмена";
    public static void main(String args[]) {
        UIManager.put("FileChooser.saveButtonText", SAVE_FILE_TEXT);
        UIManager.put("FileChooser.openButtonText", OPEN_FILE_TEXT);
        UIManager.put("FileChooser.cancelButtonText", CANCEL_FILE_TEXT);
        UIManager.put("FileChooser.fileNameLabelText", FILE_NAME_TEXT);
        UIManager.put("FileChooser.filesOfTypeLabelText", FILE_TYPES_TEXT);
        UIManager.put("FileChooser.lookInLabelText", FOLDER_NAME_TEXT);
        UIManager.put("FileChooser.saveInLabelText", SAVE_IN_FOLDER_TEXT);
        UIManager.put("FileChooser.folderNameLabelText", FOLDER_PATH_TEXT);
        UIManager.put("OptionPane.yesButtonText", YES_BUTTON_TEXT);
        UIManager.put("OptionPane.noButtonText", NO_BUTTON_TEXT);
        UIManager.put("OptionPane.cancelButtonText", CANCEL_BUTTON_TEXT);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    ex.printStackTrace();
                }
                new LifeGameMVC();
            }
        });
    }
}
