package ru.nsu.fit.g16202.ermakova;

public enum PaintType {
    NOTHING,
    FULL_REPAINT,
    PARTIAL_REPAINT,
    CELL_REPAINT,
    IMPACT_ON,
    IMPACT_OFF,
    UPDATE_IMPACT_NEIGBORS,
    UPDATE_IMPACT_STEP
}
