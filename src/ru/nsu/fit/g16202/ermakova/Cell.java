package ru.nsu.fit.g16202.ermakova;

import java.awt.*;

public class Cell {
    public boolean alive;
    public double value;
    public boolean needUpdate;
    public String impactText;
    public Point textPosition;

    Cell(boolean alive, double value) {
        this.alive = alive;
        this.value = value;
        needUpdate = true;
        impactText = null;
        textPosition = null;
    }
}
