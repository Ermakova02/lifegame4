package ru.nsu.fit.g16202.ermakova;

import java.util.TimerTask;

public class TimerTick extends TimerTask {
    private LifeModel model = null;

    public TimerTick(LifeModel model) {
        this.model = model;
    }
    @Override
    public void run() {
        model.stepGame();
    }
}
