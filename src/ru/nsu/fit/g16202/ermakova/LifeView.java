package ru.nsu.fit.g16202.ermakova;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class LifeView extends JPanel {
    private LifeGameMVC mvc = null;
    public static final int X_OFFSET = 2;
    public static final int Y_OFFSET = 2;

    public static final Color ALIVE_COLOR = new Color(144, 238, 144); // LightGreen
    public static final Color DEAD_COLOR = new Color(255, 255, 255); // White
    public static final Color GROUND_COLOR = new Color(245, 245, 220); // Beige
    public static final Color LINE_COLOR = new Color(0, 0, 0); // Black
    public static final Color IMPACT_COLOR = new Color(0, 0, 0); // Black

    private double cellK;
    private double cellH;
    private double cellB;

    BufferedImage outputImage;
    private Point repaintCell;
    private PaintType paintType;
    private boolean impactOn;
    private int panelWidth;
    private int panelHeight;

    public LifeView(LifeGameMVC mvc) {
        super();
        this.mvc = mvc;
        paintType = PaintType.FULL_REPAINT;
        repaintCell = null;
        impactOn = false;
        setLayout(new BorderLayout());
        setFocusable(true);
        prepareOutputImage();
    }

    public void prepareOutputImage() {
        double alpha = 2.0 * Math.PI / 3.0 - Math.PI / 2.0;
        cellK = (double) mvc.getModel().getCellSize();
        cellH = cellK * Math.sin(alpha);
        cellB = cellK * Math.cos(alpha);
        panelWidth = (int)((double) mvc.getModel().getColumnsNum() * 2.0 * cellB + 2.0 * X_OFFSET + 1.0);
        panelHeight = (int)((double) mvc.getModel().getRowsNum() * (cellH + cellK) + 2.0 * cellH + 2.0 * Y_OFFSET + 1.0);
        outputImage = new BufferedImage(panelWidth, panelHeight, BufferedImage.TYPE_INT_ARGB);
        setPreferredSize(new Dimension(panelWidth, panelHeight));
        setBackground(GROUND_COLOR);
    }

    private int sign(int x) {
        return (x > 0) ? 1 : (x < 0) ? -1 : 0;
    }

    private void drawBresenhamLine(Graphics g, int xstart, int ystart, int xend, int yend)
    {
        int x, y, dx, dy, incx, incy, pdx, pdy, es, el, err;
        dx = xend - xstart;
        dy = yend - ystart;
        incx = sign(dx);
        incy = sign(dy);
        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;
        if (dx > dy) {
            pdx = incx;	pdy = 0;
            es = dy;	el = dx;
        } else {
            pdx = 0;	pdy = incy;
            es = dx;	el = dy;
        }

        x = xstart;
        y = ystart;
        err = el/2;
        g.drawLine(x, y, x, y);
        for (int t = 0; t < el; t++) {
            err -= es;
            if (err < 0) {
                err += el;
                x += incx;
                y += incy;
            } else {
                x += pdx;
                y += pdy;
            }
            g.drawLine (x, y, x, y);
        }
    }

    private Color getPixelColor(Point p) {
        int rgb = outputImage.getRGB(p.x, p.y);
        Color pixelColor = new Color(rgb);
        return pixelColor;
    }

    private int getPixelColorRGB(Point p) {
        return outputImage.getRGB(p.x, p.y);
    }

    private void spanFilling(Graphics g, int x, int y, Color oldColor, Color newColor, Point cell) {
/*
 Заливка (элемент, заменяемый цвет, цвет заливки):
 1. Присвоить Q пустую очередь.
 2. Если цвет элемента - не заменяемый цвет, возврат.
 3. Поместить элемент в Q.
 4. Для каждого N из элементов Q:
 5.  Если цвет N - заменяемый цвет:
 6.   Присвоить w и e тот же элемент, что и N.
 7.   Смещать w на запад до тех пор, пока цвет w не станет отличаться от цвета "заменяемый цвет" .
 8.   Смещать e на восток до тех пор, пока цвет e не станет отличаться от цвета "заменяемый цвет".
 9.   Всем элементам между w и e придать цвет заливки.
10.   Для каждого n между w и e:
11.    Если цвет элемента к северу от n - заменяемый цвет, поместить этот элемент в Q.
       Если цвет элемента к югу от n - заменяемый цвет, поместить этот элемент в Q.
12. Продолжать цикл, пока в Q останутся элементы.
13. Возврат. {разумеется, в пп. 7, 8, а также 11 можно встретить края массива}
 */
        g.setColor(newColor);
        LinkedList<Point> queue = new LinkedList<>();
        int oldColorRGB = oldColor.getRGB();
        int newColorRGB = newColor.getRGB();
        if (oldColorRGB == newColorRGB) return;
        Point point = new Point(x, y);
        int pixelColorRGB = getPixelColorRGB(point);
        if (oldColorRGB != pixelColorRGB) return;
        queue.add(point);
        while (!queue.isEmpty()) {
            Point w = new Point(queue.element());
            Point e = new Point(queue.element());
            while (true) {
                if (w.x == 0) break;
                w.x--;
                pixelColorRGB = getPixelColorRGB(w);
                if (oldColorRGB != pixelColorRGB) {
                    w.x++;
                    break;
                }
            }
            while (true) {
                if (e.x == outputImage.getWidth() - 1) break;
                e.x++;
                pixelColorRGB = getPixelColorRGB(e);
                if (oldColorRGB != pixelColorRGB) {
                    e.x--;
                    break;
                }
            }
            g.drawLine(w.x, w.y, e.x, e.y);
            if (!mvc.getModel().isCellsAreaSet()) {
                for (int i = w.x; i <= e.x; i++)
                    mvc.getModel().setCellPosition(new Point(i, e.y), cell);
            }
            if (w.y > 0) {
                w.y--;
                for (int i = w.x; i <= e.x; i++) {
                    point = new Point(i, w.y);
                    pixelColorRGB = getPixelColorRGB(point);
                    if (oldColorRGB == pixelColorRGB) queue.add(point);
                }
            }
            if (w.y < outputImage.getHeight() - 3) {
                w.y += 2;
                for (int i = w.x; i <= e.x; i++) {
                    point = new Point(i, w.y);
                    pixelColorRGB = getPixelColorRGB(point);
                    if (oldColorRGB == pixelColorRGB) queue.add(point);
                }
            }
            queue.remove();
        }
    }

    private void paintCell(Graphics g, double xOffset, double yOffset, Color fillColor, Point cell) {
        int xPoints[] = new int[6];
        int yPoints[] = new int[6];

        xPoints[0] = (int)(0.0 + xOffset);
        xPoints[1] = (int)(cellB + xOffset);
        xPoints[2] = (int)(2.0 * cellB + xOffset);
        xPoints[3] = (int)(2.0 * cellB + xOffset);
        xPoints[4] = (int)(cellB + xOffset);
        xPoints[5] = (int)(0.0 + xOffset);

        yPoints[0] = (int)(cellH + yOffset);
        yPoints[1] = (int)(0.0 + yOffset);
        yPoints[2] = (int)(cellH + yOffset);
        yPoints[3] = (int)(cellH + cellK + yOffset);
        yPoints[4] = (int)(2.0 * cellH + cellK + yOffset);
        yPoints[5] = (int)(cellH + cellK + yOffset);

        g.setColor(LINE_COLOR);

        if (mvc.getModel().getLineThickness() == 1) {
            drawBresenhamLine(g, xPoints[0], yPoints[0], xPoints[1], yPoints[1]);
            drawBresenhamLine(g, xPoints[1], yPoints[1], xPoints[2], yPoints[2]);
            drawBresenhamLine(g, xPoints[2], yPoints[2], xPoints[3], yPoints[3]);
            drawBresenhamLine(g, xPoints[3], yPoints[3], xPoints[4], yPoints[4]);
            drawBresenhamLine(g, xPoints[4], yPoints[4], xPoints[5], yPoints[5]);
            drawBresenhamLine(g, xPoints[5], yPoints[5], xPoints[0], yPoints[0]);
        } else {
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke((float) mvc.getModel().getLineThickness()));
            g2.drawPolygon(xPoints, yPoints, 6);
            g2.setStroke(new BasicStroke(1.0f));
        }

        int x = (int)(xOffset + cellB);
        int y = (int)(yOffset + cellH + cellK / 2.0);
        spanFilling(g, x, y, getPixelColor(new Point(x, y)), fillColor, cell);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics imageGraphics = outputImage.getGraphics();
        Point cell, p;
        double shift, cellX, cellY;
        Point [] neighbors;
        Graphics2D g2d;
        FontMetrics fm;
        switch (paintType) {
            case NOTHING:
                break;
            case FULL_REPAINT:
                g.setColor(GROUND_COLOR);
                g.fillRect(0, 0, getWidth(), getHeight());
                if (!mvc.getModel().isCellsAreaSet()) {
                    mvc.getModel().setCellsAreaDimension(outputImage.getWidth(), outputImage.getHeight());
                }
                imageGraphics.setColor(GROUND_COLOR);
                imageGraphics.fillRect(0, 0, outputImage.getWidth(), outputImage.getHeight());
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        shift = (j % 2 == 1) ? cellB : 0.0;
                        cell = new Point(i, j);
                        paintCell(imageGraphics,
                                (double) X_OFFSET + shift + (double) i * 2.0 * cellB,
                                (double) Y_OFFSET + j * (cellH + cellK),
                                mvc.getModel().getCellColor(cell),
                                cell);
                        mvc.getModel().setNeedRepaint(false, cell);
                    }
                }
                mvc.getModel().setCellsAreaSet(true);
                if (impactOn) {
                    g2d = (Graphics2D) imageGraphics.create();
                    fm = g2d.getFontMetrics();
                    g2d.setColor(IMPACT_COLOR);
                    g2d.setFont(mvc.getModel().getImpactFont());
                    for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                        for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                            if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                            shift = (j % 2 == 1) ? cellB : 0.0;
                            cell = new Point(i, j);
                            Rectangle cellRect = new Rectangle(
                                    (int)((double) X_OFFSET + shift + (double) i * 2.0 * cellB),
                                    (int)((double) Y_OFFSET + j * (cellH + cellK)),
                                    (int)(cellB * 2.0),
                                    (int)(2.0 * cellH + cellK));
                            mvc.getModel().updateImpactText(cell);
                            mvc.getModel().updateImpactTextPosition(cell, cellRect, fm);
                            p = mvc.getModel().getImpactTextPosition(cell);
                            g2d.drawString(mvc.getModel().getImpactText(cell), p.x, p.y);
                        }
                    }
                    g2d.dispose();
                    impactOn = false;
                }
                paintType = PaintType.NOTHING;
                break;
            case PARTIAL_REPAINT:
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if (mvc.getModel().getNeedRepaint(new Point(i, j))) {
                            if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                            shift = (j % 2 == 1) ? cellB : 0.0;
                            cell = new Point(i, j);
                            paintCell(imageGraphics,
                                    (double) X_OFFSET + shift + (double) i * 2.0 * cellB,
                                    (double) Y_OFFSET + j * (cellH + cellK),
                                    mvc.getModel().getCellColor(cell),
                                    cell);
                            mvc.getModel().setNeedRepaint(false, cell);
                        }
                    }
                }
                mvc.getModel().setCellsAreaSet(true);
                paintType = PaintType.NOTHING;
                break;
            case CELL_REPAINT:
                shift = (repaintCell.y % 2 == 1) ? cellB : 0.0;
                cellX = (double) X_OFFSET + shift + (double) repaintCell.x * 2.0 * cellB;
                cellY = (double) Y_OFFSET + repaintCell.y * (cellH + cellK);
                paintCell(imageGraphics,
                        cellX,
                        cellY,
                        mvc.getModel().getCellColor(repaintCell),
                        repaintCell);
                mvc.getModel().setNeedRepaint(false, repaintCell);
                paintType = PaintType.NOTHING;
                break;
            case IMPACT_ON:
                g2d = (Graphics2D) imageGraphics.create();
                fm = g2d.getFontMetrics();
                g2d.setColor(IMPACT_COLOR);
                g2d.setFont(mvc.getModel().getImpactFont());
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        shift = (j % 2 == 1) ? cellB : 0.0;
                        cell = new Point(i, j);
                        Rectangle cellRect = new Rectangle(
                                (int)((double) X_OFFSET + shift + (double) i * 2.0 * cellB),
                                (int)((double) Y_OFFSET + j * (cellH + cellK)),
                                (int)(cellB * 2.0),
                                (int)(2.0 * cellH + cellK));
                        mvc.getModel().updateImpactText(cell);
                        mvc.getModel().updateImpactTextPosition(cell, cellRect, fm);
                        p = mvc.getModel().getImpactTextPosition(cell);
                        g2d.drawString(mvc.getModel().getImpactText(cell), p.x, p.y);
                    }
                }
                g2d.dispose();
                paintType = PaintType.NOTHING;
                break;
            case IMPACT_OFF:
                imageGraphics.setFont(mvc.getModel().getImpactFont());
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        cell = new Point(i, j);
                        p = mvc.getModel().getImpactTextPosition(cell);
                        imageGraphics.setColor(mvc.getModel().getCellColor(cell));
                        imageGraphics.drawString(mvc.getModel().getImpactText(cell), p.x, p.y);
                    }
                }
                paintType = PaintType.NOTHING;
                break;
            case UPDATE_IMPACT_NEIGBORS:
                imageGraphics.setFont(mvc.getModel().getImpactFont());
                neighbors = mvc.getModel().requestNeighbors(repaintCell);
                for (int i = 0; i < 13; i++) {
                    if (neighbors[i] == null) break;
                    p = mvc.getModel().getImpactTextPosition(neighbors[i]);
                    imageGraphics.setColor(mvc.getModel().getCellColor(neighbors[i]));
                    imageGraphics.drawString(mvc.getModel().getImpactText(neighbors[i]), p.x, p.y);
                }
                mvc.getModel().toggleAlive(repaintCell);
                mvc.getModel().calculateImpact();
                shift = (repaintCell.y % 2 == 1) ? cellB : 0.0;
                cellX = (double) X_OFFSET + shift + (double) repaintCell.x * 2.0 * cellB;
                cellY = (double) Y_OFFSET + repaintCell.y * (cellH + cellK);
                paintCell(imageGraphics,
                        cellX,
                        cellY,
                        mvc.getModel().getCellColor(repaintCell),
                        repaintCell);
                mvc.getModel().setNeedRepaint(false, repaintCell);
                g2d = (Graphics2D) imageGraphics.create();
                fm = g2d.getFontMetrics();
                g2d.setColor(IMPACT_COLOR);
                g2d.setFont(mvc.getModel().getImpactFont());
                for (int i = 0; i < 13; i++) {
                    if (neighbors[i] == null) break;
                    shift = (neighbors[i].y % 2 == 1) ? cellB : 0.0;
                    Rectangle cellRect = new Rectangle(
                            (int)((double) X_OFFSET + shift + (double) neighbors[i].x * 2.0 * cellB),
                            (int)((double) Y_OFFSET + neighbors[i].y * (cellH + cellK)),
                            (int)(cellB * 2.0),
                            (int)(2.0 * cellH + cellK));
                    mvc.getModel().updateImpactText(neighbors[i]);
                    mvc.getModel().updateImpactTextPosition(neighbors[i], cellRect, fm);
                    p = mvc.getModel().getImpactTextPosition(neighbors[i]);
                    g2d.drawString(mvc.getModel().getImpactText(neighbors[i]), p.x, p.y);
                }
                g2d.dispose();
                paintType = PaintType.NOTHING;
                break;
            case UPDATE_IMPACT_STEP:
                imageGraphics.setFont(mvc.getModel().getImpactFont());
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        cell = new Point(i, j);
                        p = mvc.getModel().getImpactTextPosition(cell);
                        imageGraphics.setColor(mvc.getModel().getCellColor(cell));
                        imageGraphics.drawString(mvc.getModel().getImpactText(cell), p.x, p.y);
                    }
                }
                mvc.getModel().nextStep();
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        shift = (j % 2 == 1) ? cellB : 0.0;
                        cell = new Point(i, j);
                        if (mvc.getModel().getNeedRepaint(cell)) {
                            if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                            paintCell(imageGraphics,
                                    (double) X_OFFSET + shift + (double) i * 2.0 * cellB,
                                    (double) Y_OFFSET + j * (cellH + cellK),
                                    mvc.getModel().getCellColor(cell),
                                    cell);
                            mvc.getModel().setNeedRepaint(false, cell);
                        }

                    }
                }
                g2d = (Graphics2D) imageGraphics.create();
                fm = g2d.getFontMetrics();
                g2d.setColor(IMPACT_COLOR);
                g2d.setFont(mvc.getModel().getImpactFont());
                for (int i = 0; i < mvc.getModel().getColumnsNum(); i++) {
                    for (int j = 0; j < mvc.getModel().getRowsNum(); j++) {
                        if ((j % 2 == 1) && (i == mvc.getModel().getColumnsNum() - 1)) continue;
                        shift = (j % 2 == 1) ? cellB : 0.0;
                        cell = new Point(i, j);
                        Rectangle cellRect = new Rectangle(
                                (int)((double) X_OFFSET + shift + (double) i * 2.0 * cellB),
                                (int)((double) Y_OFFSET + j * (cellH + cellK)),
                                (int)(cellB * 2.0),
                                (int)(2.0 * cellH + cellK));
                        mvc.getModel().updateImpactText(cell);
                        mvc.getModel().updateImpactTextPosition(cell, cellRect, fm);
                        p = mvc.getModel().getImpactTextPosition(cell);
                        g2d.drawString(mvc.getModel().getImpactText(cell), p.x, p.y);
                    }
                }
                g2d.dispose();
                paintType = PaintType.NOTHING;
                break;
        }
        g.drawImage(outputImage, 0, 0, null);
    }

    public void repaintCell(Point cell) {
        paintType = PaintType.CELL_REPAINT;
        repaintCell = cell;
        updateUI();
    }

    public void repaintAll(boolean impact) {
        paintType = PaintType.FULL_REPAINT;
        impactOn = impact;
        updateUI();
    }

    public void repaintPartial() {
        paintType = PaintType.PARTIAL_REPAINT;
        updateUI();
    }

    public void switchOnImpact() {
        paintType = PaintType.IMPACT_ON;
        updateUI();
    }

    public void switchOffImpact() {
        paintType = PaintType.IMPACT_OFF;
        updateUI();
    }

    public void updateImpactNeighbors(Point cell) {
        paintType = PaintType.UPDATE_IMPACT_NEIGBORS;
        repaintCell = cell;
        updateUI();
    }

    public void repaintImpactStep() {
        paintType = PaintType.UPDATE_IMPACT_STEP;
        updateUI();
    }
}
