package ru.nsu.fit.g16202.ermakova;

import java.awt.event.*;

public class LifeController implements ActionListener, MouseListener, MouseWheelListener, MouseMotionListener {
    private LifeGameMVC mvc;

    LifeController(LifeGameMVC mvc) {
        this.mvc = mvc;
        mvc.getView().addMouseListener(this);
        mvc.getView().addMouseMotionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comStr = e.getActionCommand();
        switch (comStr) {
            case LifeModel.MENU_NEW_DOCUMENT_NAME:
                mvc.getModel().newDocument();
                break;
            case LifeModel.MENU_OPEN_NAME:
                mvc.getModel().openDocument();
                break;
            case LifeModel.MENU_SAVE_NAME:
                mvc.getModel().saveDocument();
                break;
            case LifeModel.MENU_SAVE_AS_NAME:
                mvc.getModel().saveDocumentAs();
                break;
            case LifeModel.MENU_CLEAR_NAME:
                mvc.getModel().clearField();
                break;
            case LifeModel.MENU_REPLACE_NAME:
                mvc.getModel().replaceMode();
                break;
            case LifeModel.MENU_XOR_NAME:
                mvc.getModel().xorMode();
                break;
            case LifeModel.MENU_IMPACT_NAME:
                mvc.getModel().impactMode();
                break;
            case LifeModel.MENU_PARAMETERS_NAME:
                mvc.getModel().parametersDialog();
                break;
            case LifeModel.MENU_RUN_NAME:
                mvc.getModel().runGame();
                break;
            case LifeModel.MENU_STOP_NAME:
                mvc.getModel().stopGame();
                break;
            case LifeModel.MENU_STEP_NAME:
                mvc.getModel().stepGame();
                break;
            case LifeModel.MENU_RULES_NAME:
                mvc.getModel().showRules();
                break;
            case LifeModel.MENU_ABOUT_NAME:
                mvc.getModel().showAbout();
                break;
            case LifeModel.MENU_EXIT_NAME:
                mvc.getModel().offerToSaveFile();
                System.exit(0);
                break;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e.getModifiersEx() == MouseEvent.BUTTON1_DOWN_MASK) {
            mvc.getModel().processDrag(e.getPoint());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            mvc.getModel().processClick(e.getPoint());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
